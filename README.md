# Jira Gitlab linker

## Install

1. Download
2. Open Google Chrome
3. Parameters and go to the Extensions part
4. Activate the developer mode
5. Import the plugin into Google Chrome (note: open the plugin's folder to import it)

# Pictures

## Menu
![menu](https://gitlab.com/norrova/jira-gitlab-linker/-/raw/main/readme-images/menu.png)

## Merge requests
![merge requests](https://gitlab.com/norrova/jira-gitlab-linker/-/raw/main/readme-images/example1.png)

## Merge request
![merge request](https://gitlab.com/norrova/jira-gitlab-linker/-/raw/main/readme-images/example2.png)
