window.addEventListener('DOMContentLoaded', (event) => {
    console.log('DOM fully loaded and parsed');

    const form = document.querySelector('form');

    chrome.storage.sync.get(['jiraUrl', 'patternToMatch'], function(keys) {
        form.elements['jira-url'].value = keys.jiraUrl !== undefined ? keys.jiraUrl : "" ;
        form.elements['pattern-match'].value = keys.patternToMatch !== undefined ? keys.patternToMatch : "";
    });


    form.addEventListener('reset', (event) => {
        chrome.storage.sync.remove(['jiraUrl', 'patternToMatch']);
    });

    form.addEventListener('submit', (event) => {
        event.preventDefault();
        let jiraUrl = event.target.elements['jira-url'].value.trim();
        let patternToMatch = event.target.elements['pattern-match'].value.trim();
        let error = false;


        if ("" === jiraUrl || !isValidURL(jiraUrl)) {
            document.querySelector('#error-jira-url').style.display = "block";
            error = true;
        }

        if ("" === patternToMatch || !isValidRegex(patternToMatch)) {
            document.querySelector('#error-pattern-match').style.display = "block";
            console.log('here');
            error = true;
        }

        if (error) {
            return;
        }


        
        chrome.storage.sync.set({ jiraUrl });

        chrome.storage.sync.set({ patternToMatch });

        let popup = document.querySelector('#popup-success');

        popup.style.display = "block";

        setTimeout(function(){
            popup.style.display = "none";
        }, 5000);

        event.preventDefault();
    });

    function isValidURL(url) {
        try {
            new URL(url);
        } catch (e) {
            console.error(e);
            return false;
        }

        return true;
    }

    function isValidRegex(regex) {
        try {
            new RegExp(regex);
        } catch(e) {
            return false;
        }

        return true;
    }
});